import { TestBed } from '@angular/core/testing';

import { CogadminService } from './cogadmin.service';

describe('CogadminService', () => {
  let service: CogadminService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CogadminService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
