import { Injectable } from '@angular/core';

import { Config, CognitoIdentityServiceProvider } from 'aws-sdk';
import { Amplify, Auth } from 'aws-amplify';

import { Observable } from 'rxjs';

type passwordResponse = AWS.AWSError | CognitoIdentityServiceProvider.AdminSetUserPasswordResponse;

/*
* There is definetely a more elegant way to do this, this is extremely un-DRY
*
* Also getting the current credentials every method is entirely wasteful, and really the should be saved
* in a data service.
*/

@Injectable({
  providedIn: 'root'
})
export class CogadminService {

  constructor() {
  }

  private cognitoConfig = Amplify.Auth.configure();  // Data from main.ts

  private cognitoIdPoolRegion = this.cognitoConfig.region;
  private cognitoUserPoolID = this.cognitoConfig.userPoolId;

  private AWSconfig: Config;

  public getCognitoUsers = () => {
    return new Observable(observer => {
      Auth
        .currentCredentials()
        .then(user => {
            // Constructor for the global config.
            const cognitoidentityserviceprovider = this.initCognito(user);
            cognitoidentityserviceprovider.listUsers({
              UserPoolId: this.cognitoUserPoolID
            }, (err, userData) => {
              err ? console.log(err) : observer.next(userData);
            });
        })
        .catch(err => {
          observer.next({error: err});
        });
    });
  }

  public resetUserPassword = (Password: string, Username: string): Observable<passwordResponse> => {
    return new Observable(observer => {
      Auth
        .currentCredentials()
        .then( user => {
          const cognitoidentityserviceprovider = this.initCognito(user);
          cognitoidentityserviceprovider.adminSetUserPassword({
            Password,
            UserPoolId: this.cognitoUserPoolID,
            Username,
            Permanent: true
          }, (err, data) => {
            err ? observer.next({error: err}) : observer.next(data);
          });
        });
    });
  }

  public isUserAdmin = (Username: string): Observable<boolean> => {
    return new Observable(observer => {
      Auth
        .currentCredentials()
        .then( user => {
          const cognitoidentityserviceprovider = this.initCognito(user);
          cognitoidentityserviceprovider.adminListGroupsForUser({
            UserPoolId: this.cognitoUserPoolID,
            Username,
          }, (err, data) => {
            err ? observer.next(false) : observer.next(data.Groups.some(group => group.GroupName === 'admin'));
          });
        });
    });
  }

  public promoteToAdmin = ( Username: string) => {
    return new Observable(observer => {
      Auth
        .currentCredentials()
        .then( user => {
          const cognitoidentityserviceprovider = this.initCognito(user);
          cognitoidentityserviceprovider.adminAddUserToGroup({
            GroupName: 'admin',
            UserPoolId: this.cognitoUserPoolID,
            Username,
          }, (err, data) => {
            err ? observer.next({error: err}) : observer.next(data);
          });
        });
    });
  }

  public revokeAdmin = ( Username: string) => {
    return new Observable(observer => {
      Auth
        .currentCredentials()
        .then( user => {
          const cognitoidentityserviceprovider = this.initCognito(user);
          cognitoidentityserviceprovider.adminRemoveUserFromGroup({
            GroupName: 'admin',
            UserPoolId: this.cognitoUserPoolID,
            Username,
          }, (err, data) => {
            err ? observer.next({error: err}) : observer.next(data);
          });
        });
    });
  }

  private initCognito = (user): CognitoIdentityServiceProvider => {
    this.AWSconfig = new Config({
      credentials: user,    //  The whole user object goes in the config.credentials field!  Key issue.
      region: this.cognitoIdPoolRegion
    });
    return new CognitoIdentityServiceProvider(this.AWSconfig);
  }
}
