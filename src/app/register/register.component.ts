import { Component, OnInit } from '@angular/core';

import { Auth } from 'aws-amplify';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public username: string;
  public password: string;
  public email: string;
  public verificationCode: string;
  public detailsSubmit = false;
  public verificationMessage = '';
  public createErr = '';
  public verifyErr = '';
  public successMessage = '';

  constructor() { }

  ngOnInit(): void {
  }

  public submitDetails = () => {
    this.createErr = '';
    Auth.signUp({
      username: this.username,
      password: this.password,
      attributes: {
        email: this.email
      }
    })
    .then((data) => {
      console.log(data);
      this.detailsSubmit = true;
      this.verificationMessage =
        `A verification code has been sent to ${this.email}`;
    })
    .catch(err => this.createErr = err.message);
  }

  public forceVerify = () => {
    this.detailsSubmit = true;
    this.verificationMessage =
      'Please fill in the username field as well as the verification code field';
  }

  public verify = () => {
    this.verifyErr = '';
    Auth.confirmSignUp(this.username, this.verificationCode,
    {forceAliasCreation: true})
    .then(data => {
      this.detailsSubmit = false;
      this.successMessage = 'User successfully added and verified please return to the login page to login'
    })
    .catch(err => this.verifyErr = err.message);
  }

}
