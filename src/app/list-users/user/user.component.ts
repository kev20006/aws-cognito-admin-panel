// tslint:disable: no-string-literal

import { Component, OnInit, Input } from '@angular/core';

import { CogadminService } from '../../services/cogadmin.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  @Input() public user: any;

  public userIsAdmin: boolean;
  public reset = false;
  public newPassword: string;
  public passwordError: string;

  constructor(private CogAdminService: CogadminService) { }

  ngOnInit(): void {
    this.setUserAdminStatus();
  }

  private setUserAdminStatus = (): void => {
    this.CogAdminService.isUserAdmin(this.user.Username)
      .subscribe(data => this.userIsAdmin = data);
  }

  public resetHandler = (): void => {
    this.reset = !this.reset;
    this.newPassword = '';
    this.passwordError = '';
  }

  public submitResetHandler = ( user ): void => {
    this.CogAdminService.resetUserPassword(this.newPassword, user.Username).subscribe(data => {
      data['error']
        ? this.passwordError = data['error']['message']
        : this.passwordError = 'success';
    });
  }

  public userStatusHandler = ( user, action ): void => {
    const AWSAction = action === 'promote'
      ?  this.CogAdminService.promoteToAdmin
      :  this.CogAdminService.revokeAdmin;

    AWSAction(user.Username)
      .subscribe(data => {
        Object.keys(data).length === 0
          ? this.setUserAdminStatus()
          : console.log(data) ;
      });
  }
}

