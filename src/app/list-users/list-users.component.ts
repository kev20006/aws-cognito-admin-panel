import { Component, OnInit } from '@angular/core';

import { CogadminService } from '../services/cogadmin.service';
import { VolumeId } from 'aws-sdk/clients/ec2';

interface IUserList {
  Users: any[];
}

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  constructor(private cogAdminService: CogadminService) { }

  public userList: any[] | null = null;
  public isAdmin = false;

  public step: number = null;

  public setStep(index: number): void {
    this.step = index;
  }

  ngOnInit(): void {
    this.cogAdminService.getCognitoUsers().subscribe((data: IUserList) => {
      if (data.Users) {
        this.userList = data.Users;
        console.log(data.Users);
        this.isAdmin = true;
      }
    });
  }
}
