import { Component, OnInit } from '@angular/core';
import { Auth } from 'aws-amplify';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public username: string;
  public password: string;
  public loggedIn = false;
  public isAdmin = false;
  public loginErr = "";

  public login = () => {
    Auth.signIn({
      username: this.username,
      password: this.password
    }).then(userData => {
      console.log(userData);
      this.loggedIn = true;
    }).catch(err => {
      this.loginErr = err.message;
    });
  }

  public logout = () => {
    Auth.signOut()
      .then(data => {
        console.log(data);
        this.loggedIn = false;
      });
  }

}
